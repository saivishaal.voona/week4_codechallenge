import "./App.css";
import UserComponent from "./UserComponent";
import UserEmail from "./UserEmail";
import { Link, Route, Routes } from "react-router-dom";
function App() {
  return (
    <div className="App">
      <header>
        <nav class="navbar navbar-expand-lg  head">
          <div class="container-fluid">
            <a class="navbar-brand" href="#">
              User Application
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="./">
                    Home
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="bdy">
      <button type="button" class="btn btn-primary btns1"><Link className="btns" to="/userlist">USER LIST</Link></button>
      <button type="button" class="btn btn-primary btns1"><Link className="btns" to="/useremail">USER EMAIL LIST</Link></button>
      

        <div className="mainbody">
          <Routes>
            <Route path="/userlist" element={<UserComponent />}></Route>
            <Route path="/useremail" element={<UserEmail />}></Route>
          </Routes>
        </div>
      </div>
    </div>
  );
}

export default App;
